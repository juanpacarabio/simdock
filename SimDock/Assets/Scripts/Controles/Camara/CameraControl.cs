using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera Topview;
    [SerializeField] CinemachineVirtualCamera SideviewA;
    [SerializeField] CinemachineVirtualCamera SideviewB;
    [SerializeField] CinemachineVirtualCamera FrontViewA;
    [SerializeField] CinemachineVirtualCamera FrontViewB;
    [SerializeField] CinemachineVirtualCamera GoodView;

    private void OnEnable()
    {
        CameraSwitcher.Register(Topview);
        CameraSwitcher.Register(SideviewA);
        CameraSwitcher.Register(SideviewB);
        CameraSwitcher.Register(FrontViewA);
        CameraSwitcher.Register(FrontViewB);
        CameraSwitcher.Register(GoodView);
        CameraSwitcher.SwitchCamera(Topview);
    }

    private void OnDisable()
    {
        CameraSwitcher.Unregister(Topview);
        CameraSwitcher.Unregister(SideviewA);
        CameraSwitcher.Unregister(SideviewB);
        CameraSwitcher.Unregister(FrontViewA);
        CameraSwitcher.Unregister(FrontViewB);
        CameraSwitcher.Unregister(GoodView);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Q();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            E();
        }

        Space();
    }


    public void Q()
    {
        if (CameraSwitcher.IsActiveCamera(Topview))
        {
            CameraSwitcher.SwitchCamera(SideviewA);
        }
        else if (CameraSwitcher.IsActiveCamera(SideviewA))
        {
            CameraSwitcher.SwitchCamera(SideviewB);
        }
        else if (CameraSwitcher.IsActiveCamera(SideviewB))
        {
            CameraSwitcher.SwitchCamera(FrontViewA);
        }
        else if (CameraSwitcher.IsActiveCamera(FrontViewA))
        {
            CameraSwitcher.SwitchCamera(FrontViewB);
        }
        else if (CameraSwitcher.IsActiveCamera(FrontViewB))
        {
            CameraSwitcher.SwitchCamera(GoodView);
        }
        else if (CameraSwitcher.IsActiveCamera(GoodView))
        {
            CameraSwitcher.SwitchCamera(Topview);
        }
    }

    public void E()
    {
        if (CameraSwitcher.IsActiveCamera(Topview))
        {
            CameraSwitcher.SwitchCamera(GoodView);
        }
        else if (CameraSwitcher.IsActiveCamera(GoodView))
        {
            CameraSwitcher.SwitchCamera(FrontViewB);
        }
        else if (CameraSwitcher.IsActiveCamera(FrontViewB))
        {
            CameraSwitcher.SwitchCamera(FrontViewA);
        }
        else if (CameraSwitcher.IsActiveCamera(FrontViewA))
        {
            CameraSwitcher.SwitchCamera(SideviewB);
        }
        else if (CameraSwitcher.IsActiveCamera(SideviewB))
        {
            CameraSwitcher.SwitchCamera(SideviewA);
        }
        else if (CameraSwitcher.IsActiveCamera(SideviewA))
        {
            CameraSwitcher.SwitchCamera(Topview);
        }
    }

    public void Space()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CameraSwitcher.SwitchCamera(GoodView);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            CameraSwitcher.SwitchCamera(Topview);
        }
        
    }
}
