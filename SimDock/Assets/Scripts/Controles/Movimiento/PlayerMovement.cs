using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject player;
    public float speed;
    public bool canMoveLeft, canMoveRight, canMoveUp, canMoveDown, canmoveGeneral;
    
    
    public bool grabbing;
    public GameObject Grabber;
    public float ClawSpeed;
    float baseheight;


    void Start()
    {
        canmoveGeneral = true;
        baseheight = this.gameObject.transform.position.y;
        grabbing = false;
    }

    void Update()
    {
        canMoveLeft = !SomethingLeft();
        canMoveRight = !SomethingRight();
        canMoveDown = !SomethingForward();
        canMoveUp = !SomethingBack();
        if (canmoveGeneral == true)
        {
            Move();
        }

        ClawControl();

    }

    public void ClawControl()
    {
       
            if (Input.GetKey(KeyCode.Space))
            {
                if (this.gameObject.transform.position.y > 3)
                {
                    this.gameObject.transform.Translate(Vector3.down * Time.deltaTime * ClawSpeed);
                }
                Grabber.gameObject.GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                if (this.gameObject.transform.position.y < baseheight)
                {
                    this.gameObject.transform.Translate(Vector3.up * Time.deltaTime * ClawSpeed);
                    Grabber.gameObject.GetComponent<BoxCollider>().enabled = false;
                }
            }
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            Release();
        }
    }

    public void Release()
    {
        Grabber.GetComponent<Grabber>().GrabbedContainer.gameObject.transform.parent = null;
        Grabber.GetComponent<Grabber>().GrabbedContainer.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        Grabber.GetComponent<Grabber>().GrabbedContainer = null;
        grabbing = false;
    }

    public virtual void Move()
    {
        if ((Input.GetKey(KeyCode.W) && canMoveUp))
        {
            MoverAdelante();
        }
        else if ((Input.GetKey(KeyCode.S) && canMoveDown))
        {
            MoverAtras();
        }

        else if ((Input.GetKey(KeyCode.A) && canMoveLeft))
        {
            MoverIzquierda();   
        }
        else if ((Input.GetKey(KeyCode.D) && canMoveRight))
        {
            MoverDerecha();
        }else
        {
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    public void MoverAdelante()
    {
        player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 1 * speed);
    }
    public void MoverAtras()
    {
        player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 1 * -speed);
    }
    public void MoverIzquierda()
    {
        player.GetComponent<Rigidbody>().velocity = new Vector3(1 * -speed, 0, 0);
    }
    public void MoverDerecha()
    {
        player.GetComponent<Rigidbody>().velocity = new Vector3(1 * speed, 0, 0);
    }

    #region MOVEMENT-CHECKS
    internal RaycastHit downHit, upHit, leftHit, rightHit;
    public virtual bool SomethingForward()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.back);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out downHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingBack()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / -3.2f), transform.position.z), Vector3.forward);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out upHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out rightHit, transform.localScale.x / 1.8f);
    }
    public virtual bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out leftHit, transform.localScale.x / 1.8f);
    }
    #endregion


}
