using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour
{
    public GameObject Claw;
    public GameObject GrabbedContainer;
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Container"))
    //    {
    //        collision.gameObject.transform.parent = this.transform;
    //        Debug.Log("Toco un container");
    //    }
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Container"))
        {
            other.gameObject.transform.parent = Claw.transform;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            Debug.Log("Toco un container");
            GrabbedContainer = other.gameObject;
            Claw.GetComponent<PlayerMovement>().grabbing = true;
            //Claw.GetComponent<PlayerMovement>().grabbing = true;
        }
    }
}
