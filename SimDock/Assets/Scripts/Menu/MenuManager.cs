using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;

public class MenuManager : MonoBehaviour
{
    [Header("--- Pantallas de inicio ---\n")]

    [SerializeField] GameObject titleScreen;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject options;
    [SerializeField] GameObject creditos;
    [SerializeField] GameObject nuevaPartida;
    [SerializeField] GameObject ayuda;
    [SerializeField] GameObject a1;
    [SerializeField] GameObject a2;
    [SerializeField] GameObject a3;



    [Header("--- Informacion Cursor ---\n")]

    [SerializeField] Texture2D mouseTexture1;
    [SerializeField] Texture2D mouseTexture2;
    [SerializeField] Texture2D mouseTexture3;
    Vector2 mouseHotSpot;


    void Start()
    {
        showTitleScreen();
        mouseHotSpot.x = 0;
        mouseHotSpot.y = 0;
    }

    
    void Update()
    {
        spaceToContinue();   
    }

    private void spaceToContinue()
    {
        if (Input.GetKeyDown(KeyCode.Space) == true && titleScreen.activeInHierarchy==true)
        {
            showMainMenu();
        }
    }


    private void showTitleScreen()
    {
        titleScreen.SetActive(true);
        mainMenu.SetActive(false);
        options.SetActive(false);
        creditos.SetActive(false); 
        nuevaPartida.SetActive(false);
        ayuda.SetActive(false);
    }

    public void showMainMenu()
    {
        titleScreen.SetActive(false);
        mainMenu.SetActive(true);
        options.SetActive(false);
        creditos.SetActive(false);
        nuevaPartida.SetActive(false);
        ayuda.SetActive(false);

    }

    public void showOptionsMenu()
    {
        titleScreen.SetActive(false);
        mainMenu.SetActive(false);
        options.SetActive(true);
        creditos.SetActive(false);
        nuevaPartida.SetActive(false);
        ayuda.SetActive(false);

    }

    public void showCreditsMenu()
    {
        titleScreen.SetActive(false);
        mainMenu.SetActive(false);
        options.SetActive(false);
        creditos.SetActive(true);
        nuevaPartida.SetActive(false);
        ayuda.SetActive(false);

    }

    public void showNuevaPartida()
    {
        titleScreen.SetActive(false);
        mainMenu.SetActive(false);
        options.SetActive(false);
        creditos.SetActive(false);
        nuevaPartida.SetActive(true);
        ayuda.SetActive(false);
    }

    public void showAyuda()
    {
        titleScreen.SetActive(false);
        mainMenu.SetActive(false);
        options.SetActive(false);
        creditos.SetActive(false);
        nuevaPartida.SetActive(false);
        ayuda.SetActive(true);
        ayuda1();
    }

    public void ayuda1()
    {
       a1.SetActive(true);
       a2.SetActive(false);
       a3.SetActive(false);

    }
    public void ayuda2()
    {
        a1.SetActive(false);
        a2.SetActive(true);
        a3.SetActive(false);

    }
    public void ayuda3()
    {
        a1.SetActive(false);
        a2.SetActive(false);
        a3.SetActive(true);

    }

    public void demoScene()
    {
        SceneManager.LoadScene(1);
    }

    public void quitGame()
    {
        Application.Quit();
    }


    public void cursorChange1()
    {
        Cursor.SetCursor(mouseTexture1, mouseHotSpot,CursorMode.Auto);
    }
    public void cursorChange2()
    {
        Cursor.SetCursor(mouseTexture2, mouseHotSpot, CursorMode.Auto);
    }
    public void cursorChange3()
    {
        Cursor.SetCursor(mouseTexture3, mouseHotSpot, CursorMode.Auto);
    }
}
