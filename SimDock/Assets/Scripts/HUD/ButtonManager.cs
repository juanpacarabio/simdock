using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Clase encargada de administrar la funcionalidad de los botones del HUD
/// asi como de los botones de ajustes y sus animaciones.
/// </summary>
public class ButtonManager : MonoBehaviour
{
    short decoy;
    [Header("--- Pantalla Ajustes ---\n")]

    [SerializeField] GameObject ventanaAjustes;
    [SerializeField] GameObject ventanaDatos;
    [SerializeField] GameObject win;
    [SerializeField] GameObject timeUp;
    [SerializeField] TextMeshProUGUI textoHelp;
    [SerializeField] private GameObject[] BotonesSD;

    [Header("--- Pantalla Win ---\n")]
    [SerializeField] Timer timer;
    [SerializeField] TextMeshProUGUI tiempototal;
    [SerializeField] TextMeshProUGUI contEnviados;
    [SerializeField] TextMeshProUGUI contRotos;

    [Header("--- Estado de Botones ---\n")]

    [SerializeField] bool btn1Apretado = false;
    [SerializeField] bool btn2Apretado = false;
    [SerializeField] bool btn3Apretado = false;
    [SerializeField] bool btn4Apretado = false;
    [SerializeField] bool btn5Apretado = false;
    [SerializeField] bool btn6Apretado = false;
    [SerializeField] bool btn7Apretado = false;
    [SerializeField] bool btn8Apretado = false;
    [SerializeField] bool btn9Apretado = false;
    [SerializeField] bool btn10Apretado = false;
    [SerializeField] bool flechaUp = false;
    [SerializeField] bool flechaDwn = false;
    [SerializeField] bool flechaLeft = false;
    [SerializeField] bool flechaRight = false;
    [SerializeField] private Button Tip; 


    [Header("--- Referencias ---\n")]
    [SerializeField] private GameManager gameManager;
    [SerializeField] private PlayerMovement playerMovement;
    [SerializeField] private AnimacionCamion animCamion;
    [SerializeField] private AnimacionesBarco animBarco;

    [Header("--- Helper ---\n")]
    [SerializeField] private GameObject helperEyes;
    [SerializeField] private GameObject helperMouth;
    [SerializeField] private GameObject helperSadEyes;
    [SerializeField] private GameObject containerOn;




    void Start()
    {
        win = GameObject.Find("Win");
        timeUp = GameObject.Find("TimeUp");
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        timer = gameManager.GetComponent<Timer>();
        playerMovement = GameObject.Find("GruaPlaceholder").GetComponent<PlayerMovement>();
        animCamion = GameObject.FindGameObjectWithTag("VehiculoManager").GetComponent<AnimacionCamion>();
        animBarco = GameObject.FindGameObjectWithTag("VehiculoManager").GetComponent<AnimacionesBarco>();
        ventanaAjustes = GameObject.FindGameObjectWithTag("HUD_Ajustes");
        ventanaDatos = GameObject.FindGameObjectWithTag("HUD_Data");
        textoHelp = GameObject.Find("HelpText").GetComponent <TextMeshProUGUI>();
        tiempototal = GameObject.Find("tiempoTotal").GetComponent<TextMeshProUGUI>();
        contEnviados = GameObject.Find("contenedoresEnviados").GetComponent<TextMeshProUGUI>();
        contRotos = GameObject.Find("contenedoresRotos").GetComponent<TextMeshProUGUI>();
        helperEyes = GameObject.Find("HelperOjos");
        helperMouth = GameObject.Find("HelperBoca");
        helperSadEyes = GameObject.Find("HelperSadEyes");
        Tip = GameObject.FindGameObjectWithTag("Tip").GetComponent<Button>();
        containerOn = GameObject.Find("ContainerSymbolON");
        containerOn.SetActive(false);
        ventanaAjustes.SetActive(false);
        ventanaDatos.SetActive(false);
        win.SetActive(false);
        timeUp.SetActive(false);
        HelperDefaultState();
    }

    // Update is called once per frame
    void Update()
    {
        VictoriaDerrota();
        ContainerLogo();
    }

    #region BotonesHUD
    public void Boton1() // boton de datos
    {
        btn1Apretado = true;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 1!");
        ventanaDatos.SetActive(true);
    }

    public void Boton2()
    {
        btn1Apretado = false;
        btn2Apretado = true;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 2!");

    }
    public void Boton3()
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = true;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 3!");

    }
    public void Boton4()
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = true;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 4!");

    }
    public void Boton5()
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = true;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 5!");

    }
    public void Boton6() //Correo Argentino
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = true;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Tip.interactable = false;
        //POR ALGUN MOTIVO entraCamion siempre esta en true, solo la primera vez, despues se reinicia. No preguntar.
        if (animCamion.entraCamion==true && animCamion.saleCamion==false&& animCamion.tieneCargo==false || animCamion.entraCamion == false && animCamion.saleCamion == false && animCamion.tieneCargo == false)
        {
            animCamion.AnimacionEntraCamion();
            string texto = "Esta entrando el camion de carga!";
            StartCoroutine(COR_TemporalText(texto));
            return;
        }
        if (animCamion.tieneCargo ==true)
        {
            animCamion.AnimacionSaleCamion();
            string texto = "Despachando camion!";
            StartCoroutine(COR_TemporalText(texto));
        }
        else
        {
            string texto= "Falta el cargamento para el camion!";
            StartCoroutine(COR_TemporalText(texto));
        }
        Debug.Log("Apreto el boton 6!");

    }
    public void Boton7() // Boton Barco
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = true;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = false;
        Tip.interactable = false;

        if (animBarco.entraBote==false && animBarco.saleBote == false)
        {
            animBarco.AnimacionEntrarBarco();
            string texto = "Esta llegando el barco de carga!";
            StartCoroutine(COR_TemporalText(texto));

        }
        else
        {
            animBarco.AnimacionSalirBarco();
            string texto = "Se esta llendo el barco!";
            StartCoroutine(COR_TemporalText(texto));
        }
        Debug.Log("Apreto el boton 7!");

    }
    public void Boton8()
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = true;
        btn9Apretado = false;
        btn10Apretado = false;
        Debug.Log("Apreto el boton 8!");

    }
    public void Boton9() //?
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = true;
        Tip.interactable = false;
        btn10Apretado = false;

        int helpQuotes = Random.RandomRange(1,7);
        string h1 = "Manten a los transportes en constante movimiento para agilizar el trabajo!";
        string h2 = "Los contenedores verdes son fr�giles, ten cuidado con esos!!!";
        string h3 = "Asegurate de administrar bien los ordenes teniendo en cuenta el peso!";
        string h4 = "Recuerda que puedes usar las teclas Q y E para rotar la c�mara";
        string h5 = "Incomodo con el teclado? Balancea usando la interfaz para ahorrarte tiempo.";
        string h6 = "Recuerda ver el tiempo restante haciendo click en objetivos!";
        string h7 = "�Ya no quedan esperanzas? Reinicia la partida desde el boton de SimDock!!";
        if (helpQuotes == 1)
        {
            StartCoroutine(COR_TemporalText(h1));
        }
        if (helpQuotes == 2)
        {
            StartCoroutine(COR_TemporalText(h2));
        }
        if (helpQuotes == 3)
        {
            StartCoroutine(COR_TemporalText(h3));
        }
        if (helpQuotes == 4)
        {
            StartCoroutine(COR_TemporalText(h4));
        }
        if (helpQuotes == 5)
        {
            StartCoroutine(COR_TemporalText(h5));
        }
        if (helpQuotes == 6)
        {
            StartCoroutine(COR_TemporalText(h6));
        }
        if (helpQuotes == 7)
        {
            StartCoroutine(COR_TemporalText(h7));
        }


    }
    public void Boton10() //Boton SIM DOCK.
    {
        btn1Apretado = false;
        btn2Apretado = false;
        btn3Apretado = false;
        btn4Apretado = false;
        btn5Apretado = false;
        btn6Apretado = false;
        btn7Apretado = false;
        btn8Apretado = false;
        btn9Apretado = false;
        btn10Apretado = true;
        ventanaAjustes.SetActive(true);


        Debug.Log("Apreto el boton 10!");

    }

    public void FlechaArriba()
    {
        flechaUp = true;
        flechaDwn= false;
        flechaLeft = false;
        flechaRight = false;
        playerMovement.MoverAdelante();
        Debug.Log("Flecha arriba!");

    }
    public void FlechaAbajo()
    {
        flechaUp = false;
        flechaDwn = true;
        flechaLeft = false;
        flechaRight = false;
        playerMovement.MoverAtras();
        Debug.Log("Flecha abajo!");
    }
    public void FlechaDerecha()
    {
        flechaUp = false;
        flechaDwn = false;
        flechaLeft = false;
        flechaRight = true;
        playerMovement.MoverDerecha();
        Debug.Log("Flecha derecha!");
    }
    public void FlechaIzquierda()
    {
        flechaUp = false;
        flechaDwn = false;
        flechaLeft = true;
        flechaRight = false;
        playerMovement.MoverIzquierda();
        Debug.Log("Flecha zurda!");
    }

    public void BotonSalirAjustes()
    {
        SceneManager.LoadScene(0);
    }

    public void BotonOpcionesAjustes()
    {
        SceneManager.LoadScene(1);

    }

    public void BotonOpcionesPin()
    {
        DragAjustes dragAjustes = ventanaAjustes.GetComponent<DragAjustes>();
        DragAjustes dragAjustes2 = ventanaDatos.GetComponent<DragAjustes>();
        Image imagen = GameObject.Find("BTN_Pin").GetComponent<Image>();
        if (dragAjustes.enabled == true || dragAjustes2.enabled==true)
        {
            dragAjustes.enabled = false;
            dragAjustes2.enabled = false;
            imagen.color = Color.red;
        }
        else if (dragAjustes.enabled == false|| dragAjustes2.enabled == false)
        {
            dragAjustes.enabled = true;
            dragAjustes2.enabled = true;
            imagen.color = Color.green;
        }
        
    }

    public void BotonVolverAjustes()
    {
        ventanaAjustes.SetActive(false);
        ventanaDatos.SetActive(false);
    }

    private void VictoriaDerrota()
    {
        if (gameManager.victoria == true)
        {
            win.SetActive(true);
            contEnviados.text = gameManager.ContainersEnviados.ToString();
            contRotos.text = gameManager.ContainersRotos.ToString();
            float minutos = Mathf.FloorToInt(timer.tiempoRestante / 60);
            float segundos = Mathf.FloorToInt(timer.tiempoRestante % 60);
            tiempototal.text = string.Format("{0:00} : {1:00}", minutos, segundos);
        }
        if (gameManager.tiempoagotado == true)
        {
            timeUp.SetActive(true);
        }
    }
    private void ContainerLogo()
    {
        if (playerMovement.grabbing == false)
        {
            containerOn.SetActive(false);
        }
        if (playerMovement.grabbing == true)
        {
            containerOn.SetActive(true);
        }
    }

    private void HelperDefaultState()
    {
        helperEyes.SetActive(false);
        helperMouth.SetActive(false);
        helperSadEyes.SetActive(false);
    }

    IEnumerator COR_TemporalText(string texto)
    {

        textoHelp.text = texto;
        helperMouth.SetActive(true);
        yield return new WaitForSeconds(5f);
        helperMouth.SetActive(false);
        textoHelp.text = "Hola bienvenido a Sim Dock, soy el Dock Helper y estoy aqu� para ayudarte!";
        Tip.interactable = true;
        yield break;

    }

    #endregion

}
