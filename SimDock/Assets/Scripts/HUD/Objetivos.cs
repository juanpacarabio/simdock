using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Objetivos : MonoBehaviour
{
    [Header("=== Elementos Necesarios ===")]
    [Space(10)]
    [SerializeField] GameManager gameManager;
    [SerializeField] private Timer timer;
    [SerializeField] private TextMeshProUGUI contadorReloj;
    [SerializeField] private TextMeshProUGUI contadorInicialContenedores;
    [SerializeField] private TextMeshProUGUI contadorTotalContenedores;
    [SerializeField] private TextMeshProUGUI contadorContenedoresRotos;
    void Start()
    {
        gameManager= GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        timer = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Timer>();
    }

    // Update is called once per frame
    void Update()
    {
        setTimer();
        setContainerData();
    }

    private void setTimer()
    {
        float minutos = Mathf.FloorToInt(timer.tiempoRestante/ 60);
        float segundos = Mathf.FloorToInt(timer.tiempoRestante % 60);
        contadorReloj.text = string.Format("{0:00} : {1:00}", minutos, segundos);

    }

    private void setContainerData()
    {
        contadorInicialContenedores.text = gameManager.ContainersEnviados.ToString();
        contadorTotalContenedores.text = "/ "+ gameManager.ContainersTotal.ToString();
        contadorContenedoresRotos.text = gameManager.ContainersRotos.ToString();    
    }
}
