using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{
    [SerializeField] internal bool victoria = false;
    [SerializeField] internal bool tiempoagotado = false;
    public GameObject[] Containers;
    public int ContainersTotal;
    public int ContainersEnviados;
    public int ContainersRotos;
    Timer Temporizador;
    void Start()
    {
        Containers = GameObject.FindGameObjectsWithTag("Container");
        List<GameObject> list = new List<GameObject>(Containers);
        list = list.Distinct().ToList();
        Containers = list.ToArray();
        ContainersTotal = Containers.Length;
        Temporizador = this.gameObject.GetComponent<Timer>();
    }
    void Update()
    {
        Debug.Log("Rompiste " + ContainersRotos + " containers");

        if (ContainersEnviados + ContainersRotos == ContainersTotal)
        {
            Victoria();
        }
    }

    public void Victoria()
    {
        Temporizador.countingDown = false;
        victoria = true;
        Debug.Log("Ganaste!!!!!!!!!!!!!!!!!!!");
    }

    public void TimeUp()
    {
        Debug.Log("Se acabo el tiempo");
        tiempoagotado = true;
    }
}
