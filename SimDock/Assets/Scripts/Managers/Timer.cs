using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float tiempoBase;
    public float tiempoRestante;
    public GameManager manager;
    public bool countingDown;
    private void Start()
    {
        tiempoRestante = tiempoBase;
        manager = this.gameObject.GetComponent<GameManager>();
        countingDown = true;
    }
    private void LateUpdate()
    {
        if (countingDown == true)
        {
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;
                Debug.Log(tiempoRestante);
            }
            else if (tiempoRestante <= 0)
            {
                manager.TimeUp();
                countingDown = false;
            }
        }
    }
}
