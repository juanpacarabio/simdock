using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionCamion : MonoBehaviour
{
    [Header("=== Elementos de Animacion ===")]
    [Space(15)]
    [SerializeField] internal Animator anim;
    [SerializeField] internal GameObject contenedor;
    [SerializeField] internal bool entraCamion = false;
    [SerializeField] internal bool saleCamion = false;
    [SerializeField] internal bool tieneCargo = false;


    void Start()
    {
        anim = GameObject.FindGameObjectWithTag("CargoTruck").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (anim == null)
        {
            anim = GameObject.FindGameObjectWithTag("CargoTruck").GetComponent<Animator>();
        }
        else
        {
            return;
        }

        try
        {
            contenedor = GameObject.Find("ContainerTruckInstantiate");
            Destroy(contenedor);
            tieneCargo= false;
        }
        catch 
        {
            return;
        }
    }

    private void SetAnimationBools()
    {
        anim.SetBool("entraCamion", entraCamion);
        anim.SetBool("saleCamion", saleCamion);
    }

    public void AnimacionEntraCamion()
    {
        StartCoroutine(COR_EntradaCamion());
    }

    public void AnimacionSaleCamion()
    {
        if (tieneCargo == true)
        {
            StartCoroutine(COR_SalidaCamion());
        }
        else { return; }
    }

    IEnumerator COR_EntradaCamion()
    {
        entraCamion = true;
        SetAnimationBools();
        yield return new WaitForSeconds(4.8f);
        entraCamion = false;
        SetAnimationBools();
        yield break;
    }
    IEnumerator COR_SalidaCamion()
    {
        saleCamion=true;
        SetAnimationBools();
        yield return new WaitForSeconds(4.8f);
        saleCamion = false;
        SetAnimationBools();
        yield break;
    }

}
