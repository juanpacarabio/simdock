using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionesBarco : MonoBehaviour
{
    [Header("=== Elementos de Animacion ===")]
    [Space(15)]
    [SerializeField] internal Animator anim;
    [SerializeField] internal bool entraBote=false;
    [SerializeField] internal bool saleBote = false;


    void Start()
    {
        anim = GameObject.FindGameObjectWithTag("CargoShip").GetComponent<Animator>();
    }
    private void Update()
    {
        if (anim == null)
            anim = GameObject.FindGameObjectWithTag("CargoShip").GetComponent<Animator>();
        else
            return;
    }
    private void setAnimationBools()
    {
        anim.SetBool("entraBote", entraBote);
        anim.SetBool("saleBote", saleBote);
    }

    public void AnimacionEntrarBarco()
    {
        StartCoroutine(COR_entraBote());
    }
    public void AnimacionSalirBarco()
    {
        StartCoroutine(COR_salidaBote());
    }

    IEnumerator COR_entraBote()
    {
        entraBote = true;
        setAnimationBools();
        yield return new WaitForSeconds(4.8f);
        entraBote = false;
        setAnimationBools();
        yield break;
    }

    IEnumerator COR_salidaBote()
    {
        saleBote = true;
        setAnimationBools();
        yield return new WaitForSeconds(4.8f);
        saleBote = false;
        setAnimationBools();
        yield break;
    }
}
