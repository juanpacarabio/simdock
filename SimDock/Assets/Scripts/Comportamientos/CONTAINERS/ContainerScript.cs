using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerScript : MonoBehaviour
{
    public int peso; // 0 = fr�gil, 1 = lig�ro, 2 = mediano, 3 = pesado
    public int peso_total;
    public bool roto = false;
    public GameObject top_sensor;
    public GameObject bottom_container;
    public GameObject Normal_Variant;
    public GameObject Broken_Variant;
    public GameObject GameManager;
    public GameManager manager;
    public GameObject Barco;
    void Start()
    {
        peso_total = peso;
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        manager = GameManager.GetComponent<GameManager>();
    }

    void Update()
    {
        if (peso < 3)
        {
            if (roto == false)
            {
                if (peso_total > peso*2)
                {
                    Romperse();
                }
            }
        }
        if (this.gameObject.transform.position.y < -3)
        {
            
            if (roto == false)
            {
                manager.ContainersRotos++;
            }

            Destroy(this.gameObject);
        }
    }

    public void AvisarPeso()
    {
        bottom_container.GetComponent<ContainerScript>().peso_total = bottom_container.GetComponent<ContainerScript>().peso + peso_total;
    }

    public void AliviarPeso()
    {
        bottom_container.GetComponent<ContainerScript>().peso_total = bottom_container.GetComponent<ContainerScript>().peso_total - peso_total;
    }

    public void Romperse()
    {
        Normal_Variant.SetActive(false);
        Broken_Variant.SetActive(true);
        roto = true;
        manager.ContainersRotos++;
    }

    public void EstaEnBarco()
    {
        top_sensor.gameObject.GetComponent<Sensor>().EstaEnBarco = true;
        top_sensor.gameObject.GetComponent<Sensor>().Barco = Barco;
    }
}
