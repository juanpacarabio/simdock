using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    public GameObject Container;
    public ContainerScript PropiedadesPropias;
    public ContainerScript PropiedadesAjenas;
    public GameObject Barco;
    public bool EstaEnBarco = false;
    private void Start()
    {
        PropiedadesPropias = Container.GetComponent<ContainerScript>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Container"))
        {
            PropiedadesAjenas = other.GetComponent<ContainerScript>();
            //PropiedadesPropias.peso_total = PropiedadesPropias.peso_total + PropiedadesAjenas.peso_total;
            PropiedadesAjenas.bottom_container = Container;
            PropiedadesAjenas.AvisarPeso();
            if (PropiedadesPropias.bottom_container != null)
            {
                PropiedadesPropias.AvisarPeso();
            }

            if (EstaEnBarco == true)
            {
                other.gameObject.transform.parent = this.gameObject.transform.parent;
                other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                other.gameObject.GetComponent<ContainerScript>().Barco = Barco;
                other.gameObject.GetComponent<ContainerScript>().EstaEnBarco();
                Barco.GetComponent<BarcoScript>().Cargo.Add(other.gameObject);

                Barco.gameObject.GetComponent<BarcoScript>().Cargo = Barco.gameObject.GetComponent<BarcoScript>().Cargo.Distinct().ToList();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PropiedadesPropias.peso_total = PropiedadesPropias.peso_total - PropiedadesAjenas.peso_total;
        if (PropiedadesPropias.bottom_container != null)
        {
            PropiedadesPropias.AvisarPeso();
        }
        other.gameObject.GetComponent<ContainerScript>().bottom_container = null;
    }
}
