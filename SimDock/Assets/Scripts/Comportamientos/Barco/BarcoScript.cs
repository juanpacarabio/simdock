using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarcoScript : MonoBehaviour
{
    public GameObject Sensor;
    public List<GameObject> Cargo;
    GameObject GManager;
    GameManager Manager;

    private void Start()
    {
        GManager = GameObject.FindGameObjectWithTag("GameManager");
        Manager = GManager.GetComponent<GameManager>();
    }
    public void InstantiateNewBoat()
    {
        Transform spawnPosition;
        spawnPosition = GameObject.Find("BoatSpawnPosition").transform;
        Instantiate(this.gameObject,spawnPosition.position,spawnPosition.rotation);
    }
    public void DestroyCurrentBoat()
    {
        Destroy(this.gameObject);
    }

    public void DestroyContainers()
    {
        foreach(GameObject container in Cargo)
        {
            if (container.GetComponent<ContainerScript>().roto == false)
            {
                Manager.ContainersEnviados++;
            }
            Destroy(container.gameObject);
        }
        Cargo.Clear();
    }
}
