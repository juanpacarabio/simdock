using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Plataforma : MonoBehaviour
{
    public GameObject Barco;
    void Start()
    {
        Barco = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Container"))
        {
            other.gameObject.transform.parent = Barco.transform;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            other.gameObject.GetComponent<ContainerScript>().EstaEnBarco();
            Barco.gameObject.GetComponent<BarcoScript>().Cargo.Add(other.gameObject);
            other.gameObject.GetComponent<ContainerScript>().Barco = Barco;
            
            Barco.gameObject.GetComponent<BarcoScript>().Cargo = Barco.gameObject.GetComponent<BarcoScript>().Cargo.Distinct().ToList();
        }
    }
}
