using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosCamion : MonoBehaviour
{
    public GameObject Cargo;
    public void InstantiateNewTruck()
    {
        Transform spawnPosition;
        spawnPosition = GameObject.Find("TruckSpawnPosition").transform;
        //Buscar la manera de eliminar el contenedor antes de instanciar el nuevo camion antes.
        GameObject truck= Instantiate(this.gameObject, spawnPosition.position, spawnPosition.rotation);
        //GameObject contenedorBorrar = truck.transform.Find("ContainerTruckInstantiate").gameObject;
        //Destroy(contenedorBorrar);
    }
    public void DestroyCurrentTruck()
    {
        Destroy(this.gameObject);
    }

    public void AddToCounter()
    {
        GameObject GManager = GameObject.FindGameObjectWithTag("GameManager");
        GameManager manager = GManager.GetComponent<GameManager>();

        if (Cargo.GetComponent<ContainerScript>().roto == false )
        {
            manager.ContainersEnviados++;
        }

        Destroy(Cargo); Cargo = null;
    }
}
