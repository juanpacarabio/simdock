using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamionScript : MonoBehaviour
{
    [Header("=== Elementos Necesarios ===")]
    [Space(15)]
    [SerializeField] private AnimacionCamion animacionCamion;
    public GameObject Camion;
    public EventosCamion ComportamientoCamion;

    private void Start()
    {
       animacionCamion = GameObject.Find("== VehiculoManager ==").GetComponent<AnimacionCamion>();
        ComportamientoCamion = Camion.GetComponent<EventosCamion>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Container"))
        {
            animacionCamion.tieneCargo= true;
            ComportamientoCamion.Cargo = collision.gameObject;
        }
        else 
        {
            animacionCamion.tieneCargo = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Container"))
        {
            animacionCamion.tieneCargo = true;
            Transform container = other.gameObject.transform;
            container.parent = this.gameObject.transform;
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            GameObject GOcontainer = other.gameObject;
            GOcontainer.name = "ContainerTruckInstantiate";
            ComportamientoCamion.Cargo = other.gameObject;
        }
        else
        {
            animacionCamion.tieneCargo = false;
        }
    }
}
