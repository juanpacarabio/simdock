using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoScript : MonoBehaviour
{
    public GameObject[] ContainerType; // 0 = Normal, 1 = Medio, 2 = Pesado, 3 = Fr�gil, 4 = Hazardous.
    public int tipo;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void GenerateSelf()
    {
        tipo = Random.Range(0, 4);
        foreach (GameObject a in ContainerType)
        {
            a.SetActive(false);
        }
        ContainerType[tipo].SetActive(true);
    }

    public void TopOfHeavy()
    {
        tipo = Random.Range(0, 4);
        
        foreach (GameObject a in ContainerType)
        {
            a.SetActive(false);
        }
        ContainerType[tipo].SetActive(true);
    }

    public void TopOfMedium()
    {
        int option = Random.Range(0, 3);
        if (option == 0)
        {
            tipo = 0;
        }
        else if (option == 1)
        {
            tipo = 3;
        }
        else if (option == 2)
        {
            tipo = 4;
        }else if (option == 3)
        {
            tipo = 1;
        }
        foreach (GameObject a in ContainerType)
        {
            a.SetActive(false);
        }
        ContainerType[tipo].SetActive(true);
    }

    public void TopOfLight()
    {
        int option = Random.Range(0, 2);
        if (option == 0)
        {
            tipo = 0;
        }
        else if (option == 1)
        {
            tipo = 3;
        }
        else if (option == 2)
        {
            tipo = 4;
        }
        foreach (GameObject a in ContainerType)
        {
            a.SetActive(false);
        }
        ContainerType[tipo].SetActive(true);
    }

    public void GenerateHazardous()
    {
        tipo = 4;
        foreach (GameObject a in ContainerType)
        {
            a.SetActive(false);
        }
        ContainerType[tipo].SetActive(true);
    }
}
